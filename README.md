## Getting Started

1. npm install -g generator-lucille
2. yo lucille
3. grunt
4. Navigate to http://localhost:9000/


## What's Included

This Yeoman generator is a front end start up package utilising a sass/flex based grid created by https://github.com/digitaledgeit

## License

[MIT License](http://en.wikipedia.org/wiki/MIT_License)


[Yeoman]: http://yeoman.io
